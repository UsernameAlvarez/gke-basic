terraform {
  backend "gcs" {
    bucket = "tf-state-backup-terraform"
    credentials = "accounts.json"
  }
}