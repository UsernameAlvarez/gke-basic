locals {
  region  = "us-central1"
  zone    = "us-central1-c"
}

# Specify the GCP Provider
provider "google" {
  credentials = file("accounts.json")
  project     = "stashconsulting"
  region      = local.region
  zone        = local.zone
}

resource "google_container_cluster" "gke-cluster" {
  name               = "my-first-gke-cluster"
  network            = "default"
  initial_node_count = 3
}

resource "google_container_node_pool" "extra-pool" {
  name               = "extra-node-pool"
  cluster            = google_container_cluster.gke-cluster.name
  initial_node_count = 1
}
